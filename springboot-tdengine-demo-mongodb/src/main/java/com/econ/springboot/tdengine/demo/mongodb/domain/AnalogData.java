package com.econ.springboot.tdengine.demo.mongodb.domain;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Accessors(chain = true)
@Document("analog_data")
public class AnalogData {

    /**
     * 时间戳，主键
     */
    //private Timestamp ts;

    private String id;

    private String pid;

    private Integer m;

    private String v;
}
