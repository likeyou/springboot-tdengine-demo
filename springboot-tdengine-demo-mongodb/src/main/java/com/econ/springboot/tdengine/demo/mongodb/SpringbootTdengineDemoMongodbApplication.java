package com.econ.springboot.tdengine.demo.mongodb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
public class SpringbootTdengineDemoMongodbApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootTdengineDemoMongodbApplication.class, args);
    }

}
