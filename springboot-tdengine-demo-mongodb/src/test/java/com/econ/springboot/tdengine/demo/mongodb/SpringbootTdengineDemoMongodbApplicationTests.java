package com.econ.springboot.tdengine.demo.mongodb;

import com.econ.springboot.tdengine.demo.mongodb.domain.AnalogData;
import com.econ.springboot.tdengine.demo.mongodb.domain.Weather;
import com.econ.springboot.tdengine.demo.mongodb.service.WeatherService;
import com.sun.corba.se.spi.ior.ObjectId;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import javax.annotation.Resource;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

@Slf4j
@SpringBootTest
class SpringbootTdengineDemoMongodbApplicationTests {

    @Autowired
    private WeatherService weatherService;
    @Autowired
    private MongoTemplate mongoTemplate;
    @Resource(name = "taskExecutor")
    ThreadPoolTaskExecutor threadPoolTaskExecutor;

    private Random random = new Random(System.currentTimeMillis());
    private String[] locations = {"北京", "上海", "广州", "深圳", "天津"};

    // 这里是种子字母 当然如果需要可以加上数字 已经把大小写区分不明显的去掉了
    public static Character[] words = new Character[]{'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l' ,'m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
            'A', 'B', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};


    @Test
    void contextLoads() {
    }

    @Test
    void testInsert() {
        long totalTime = 0;
        for(int j = 0;j < 1;j++) {
            long ts = System.currentTimeMillis();
            long thirtySec = 1000 * 30;
            List<Weather> list = new LinkedList();
            Weather weather = null;
            for (int i = 0; i < 1; i++) {
                weather = new Weather();
                weather.setTs(new Timestamp(ts + (thirtySec * i)));
                weather.setTemperature(30 * random.nextFloat());
                weather.setHumidity((float) random.nextInt(100));
                weather.setLocation(locations[random.nextInt(locations.length)]);
                weather.setGroupId(i % locations.length);
                list.add(weather);
            }
            long start = System.currentTimeMillis();
            weatherService.insert(list);
            long end = System.currentTimeMillis();
            System.out.println("插入数据用时: " + (end-start));
            totalTime = totalTime + (end - start);
        }
        System.out.println("插入数据总用时：" + totalTime);
    }

    @Test
    void testInsertAnalogData() {
        long totalTime = 0;
        Random random = new Random();
        for(int j = 0; j<1; j++) {
            List<AnalogData> list = Lists.newArrayList();
            for (int i=0; i< 10; i++) {
                String pid = getRandomWords(3, 2) + "." + getRandomWords(3, 2);
                AnalogData s = new AnalogData().setId(pid + "." + System.currentTimeMillis() + random.nextInt(100)).setPid(pid).setM(100).setV("1.00");
                list.add(s);
            }
            long start = System.currentTimeMillis();
            mongoTemplate.insert(list, "analog_data");
            long end = System.currentTimeMillis();
            System.out.println("耗时：" + (end-start));
            totalTime += (end - start);
        }
        log.info("总耗时：{}", totalTime);
    }

    /**
     * 获取随机字母组合
     * begin 最小长度
     * offset 最小长度之后随机增加长度区间
     * @return
     */
    public static String getRandomWords(Integer begin, Integer offset) {
        //创建random 需要一个种子 同样的种子会出现固定顺序的random
        // 突发奇想用了时间戳
        Random random = new Random(System.currentTimeMillis());
        // 计算最终返回长度 这个方法是包左不包右的所以+1
        int i = random.nextInt(offset + 1) + begin;
        // 返回结果预存集合
        List<Character> results = new ArrayList<>();
        while(results.size() < i) {
            // 数组中取出一个随机索引 以及元素
            int index=(int)(Math.random()*words.length);
            results.add(words[index]);
        }
        // list 转character数组
        Character[] array = results.toArray(new Character[]{});
        // character 数组转char
        char[] chars = ArrayUtils.toPrimitive(array);
        // 返回结果
        return new String(chars);
    }

    @Test
    @Async("taskExecutor")
    void testInsertByThreadPoolTask() throws InterruptedException {
        int count = 5000;
        CountDownLatch countDownLatch = new CountDownLatch(count);
        long totalStart = System.currentTimeMillis();
        for(int i = 0;i < 1000;i++) {
            threadPoolTaskExecutor.execute(() -> {
                try {
                    Random random = new Random();
                    List<AnalogData> list = Lists.newArrayList();
                    for (int j = 0; j < count; j++) {
                        String pid = getRandomWords(3, 2) + "." + getRandomWords(3, 2);
                        AnalogData s = new AnalogData().setId(pid + "." + System.currentTimeMillis() + random.nextInt(100)).setPid(pid).setM(100).setV("1.00");
                        list.add(s);
                    }
                    long start = System.currentTimeMillis();
                    //mongoTemplate.insert(list, "analog_data");
                    mongoTemplate.insertAll(list);
                    long end = System.currentTimeMillis();
                    System.out.println(Thread.currentThread().getName() + "耗时：" + (end - start));
                } finally {
                    countDownLatch.countDown();
                }
            });
        }
        System.out.println("等待子线程完成~~~");
        countDownLatch.await();
        long totalEnd = System.currentTimeMillis();
        System.out.println("总用时：" + (totalEnd-totalStart));
        System.out.println("主线程结束");
    }
}
