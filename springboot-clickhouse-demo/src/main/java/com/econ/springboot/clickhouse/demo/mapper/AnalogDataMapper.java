package com.econ.springboot.clickhouse.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.econ.springboot.clickhouse.demo.domain.AnalogData;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface AnalogDataMapper extends BaseMapper<AnalogData> {

    @Insert("<script>insert into analog_data_20231008 VALUES <foreach collection='list' item='obj' separator=','>(#{obj.id}, #{obj.pid}, #{obj.t}, #{obj.v}, #{obj.dqf})</foreach></script>")
    int insertBatch(@Param("list") List<AnalogData> list);

    @Insert("insert into analog_data_20231008 VALUES (#{obj.id}, #{obj.pid}, #{obj.t}, #{obj.v}, #{obj.dqf})")
    int insertOne(@Param("obj") AnalogData obj);

    @Results({
            @Result(property = "ts", column = "last_row(ts)"),
            @Result(property = "id", column = "last_row(id)"),
            @Result(property = "m", column = "last_row(m)"),
            @Result(property = "v", column = "last_row(v)"),
            @Result(property = "pid", column = "last_row(pid)")
    })
    @Select("select last_row(*),last_row(pid) from analog_data group by pid")
    List<AnalogData> selectLastRowGroupByPid();
}
