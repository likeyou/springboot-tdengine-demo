package com.econ.springboot.clickhouse.demo;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.econ.springboot.clickhouse.demo.mapper")
public class SpringbootClickhouseDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootClickhouseDemoApplication.class, args);
    }

}
