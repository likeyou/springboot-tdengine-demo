package com.econ.springboot.clickhouse.demo.domain;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class AnalogData {

    /**
     * 时间戳，主键
     */
    //private Timestamp ts;

    private String id;

    private String pid;

    private Long t;

    private String v;

    private Short dqf;
}
