package com.econ.springboot.tdengine.demo.multi.datasource.mybatis.plus;

import com.econ.springboot.tdengine.demo.multi.datasource.mybatis.plus.dao.UserMapper;
import com.econ.springboot.tdengine.demo.multi.datasource.mybatis.plus.dao.WeatherMapper;
import com.econ.springboot.tdengine.demo.multi.datasource.mybatis.plus.domain.User;
import com.econ.springboot.tdengine.demo.multi.datasource.mybatis.plus.domain.Weather;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@Slf4j
@SpringBootTest
class SpringbootTdengineDemoMultiDatasourceMybatisPlusApplicationTests {
    @Autowired
    UserMapper userMapper;
    @Autowired
    WeatherMapper weatherMapper;

    @Test
    void contextLoads() {
    }

    @Test
    void test1(){
        List<User> users = userMapper.selectList(null);
        log.info("user count: {}", users.size());
        List<Weather> weathers = weatherMapper.selectList(null);
        log.info("weathers count: {}", weathers.size());
    }

}
