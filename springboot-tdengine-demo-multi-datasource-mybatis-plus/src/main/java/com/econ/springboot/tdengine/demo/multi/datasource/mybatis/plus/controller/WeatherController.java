package com.econ.springboot.tdengine.demo.multi.datasource.mybatis.plus.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.econ.springboot.tdengine.demo.multi.datasource.mybatis.plus.domain.Weather;
import com.econ.springboot.tdengine.demo.multi.datasource.mybatis.plus.service.WeatherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/weather")
@RestController
public class WeatherController {

    @Autowired
    private WeatherService weatherService;

    /**
     * create database and table
     *
     * @return
     */
    /*@GetMapping("/init")
    public int init() {
        return weatherService.init();
    }

    *//**
     * Pagination Query
     *
     * @param limit
     * @param offset
     * @return
     *//*
    @GetMapping("/{limit}/{offset}")
    public List<Weather> queryWeather(@PathVariable Long limit, @PathVariable Long offset) {
        return weatherService.query(limit, offset);
    }

    *//**
     * upload single weather info
     *
     * @param temperature
     * @param humidity
     * @return
     *//*
    @PostMapping("/{temperature}/{humidity}")
    public int saveWeather(@PathVariable float temperature, @PathVariable float humidity) {
        return weatherService.save(temperature, humidity);
    }

    @GetMapping("/count")
    public int count() {
        return weatherService.count();
    }

    @GetMapping("/subTables")
    public List<String> getSubTables() {
        return weatherService.getSubTables();
    }

    @GetMapping("/avg")
    public List<Weather> avg() {
        return weatherService.avg();
    }


    @GetMapping("/plus")
    public List<Weather> plus() {
        return weatherService.plus();
    }

    *//**
     * 测试mybatis 分页
     * @param pageSize
     * @param limit
     * @return
     *//*
    @GetMapping("/page/{pageSize}/{limit}")
    public IPage<Weather> page(@PathVariable("pageSize") Long pageSize,@PathVariable("limit") Long limit) {
        IPage<Weather> weatherIpage = new Page<Weather>(pageSize,limit);
        return weatherService.page(weatherIpage);
    }

    @GetMapping("/mypage/{pageSize}/{limit}")
    public IPage<Weather> table(@PathVariable("pageSize") Long pageSize,@PathVariable("limit") Long limit,@RequestParam String table) {
        IPage<Weather> weatherIpage = new Page<Weather>(pageSize,limit);
        return weatherService.myPage(weatherIpage,table);
    }*/

}
