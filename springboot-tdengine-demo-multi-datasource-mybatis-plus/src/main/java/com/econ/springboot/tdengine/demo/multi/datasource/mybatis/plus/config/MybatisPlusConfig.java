package com.econ.springboot.tdengine.demo.multi.datasource.mybatis.plus.config;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author LHT
 * @version 1.0
 * @date 2021/4/25 2:24 PM
 * @description
 */
@Configuration
public class MybatisPlusConfig {

    /**
     * 配置分页插件，分页插件默认不支持tdengine类型，但是经过测试Mysql数据库跟Tdengine使用方法一致，所以下边配置成DbType.MYSQL
     * @return
     */
    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();

        interceptor.addInnerInterceptor(new PaginationInnerInterceptor(DbType.MYSQL));
        return interceptor;
    }

}