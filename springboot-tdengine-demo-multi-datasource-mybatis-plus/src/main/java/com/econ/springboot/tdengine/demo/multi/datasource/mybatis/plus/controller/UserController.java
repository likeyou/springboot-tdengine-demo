package com.econ.springboot.tdengine.demo.multi.datasource.mybatis.plus.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.econ.springboot.tdengine.demo.multi.datasource.mybatis.plus.domain.User;
import com.econ.springboot.tdengine.demo.multi.datasource.mybatis.plus.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author LHT
 * @version 1.0
 * @date 2021/4/25 3:42 PM
 * @description
 */
@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    UserService userService;

    @GetMapping("/page/{pageSize}/{limit}")
    public IPage<User> selectUser(@PathVariable("pageSize") Long pageSize, @PathVariable("limit") Long limit) {
        IPage<User> userIpage = new Page<>(pageSize, limit);
        return userService.selectUser(userIpage);
    }

}
