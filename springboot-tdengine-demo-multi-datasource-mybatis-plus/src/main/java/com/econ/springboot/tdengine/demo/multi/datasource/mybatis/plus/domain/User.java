package com.econ.springboot.tdengine.demo.multi.datasource.mybatis.plus.domain;

/**
 * @author LHT
 * @version 1.0
 * @date 2021/4/25 3:46 PM
 * @description
 */
import lombok.Data;

@Data
public class User {
    private Long id;
    private String name;
    private int age;
    private String email;
}