package com.econ.springboot.tdengine.demo.multi.datasource.mybatis.plus.dao;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.econ.springboot.tdengine.demo.multi.datasource.mybatis.plus.domain.Weather;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;


@DS("tdengine")
public interface WeatherMapper extends BaseMapper<Weather> {
}
