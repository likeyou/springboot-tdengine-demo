package com.econ.springboot.tdengine.demo.multi.datasource.mybatis.plus;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@MapperScan(basePackages = {"com.econ.springboot.tdengine.demo.multi.datasource.mybatis.plus.dao"})
@SpringBootApplication
public class SpringbootTdengineDemoMultiDatasourceMybatisPlusApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootTdengineDemoMultiDatasourceMybatisPlusApplication.class, args);
    }

}
