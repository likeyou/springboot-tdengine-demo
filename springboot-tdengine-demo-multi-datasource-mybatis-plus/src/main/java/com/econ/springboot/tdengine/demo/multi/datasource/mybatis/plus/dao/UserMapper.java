package com.econ.springboot.tdengine.demo.multi.datasource.mybatis.plus.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.econ.springboot.tdengine.demo.multi.datasource.mybatis.plus.domain.User;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @author LHT
 * @version 1.0
 * @date 2021/4/25 3:42 PM
 * @description
 */
public interface UserMapper extends BaseMapper<User> {
}
