package com.econ.springboot.tdengine.demo.multi.datasource.mybatis.plus.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.econ.springboot.tdengine.demo.multi.datasource.mybatis.plus.dao.UserMapper;
import com.econ.springboot.tdengine.demo.multi.datasource.mybatis.plus.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author LHT
 * @version 1.0
 * @date 2021/4/25 3:44 PM
 * @description
 */
@Service
public class UserService {
    @Autowired
    UserMapper userMapper;

    public IPage<User> selectUser(IPage<User> iPage) {
        return userMapper.selectPage(iPage, null);
    }
}
