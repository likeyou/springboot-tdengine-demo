package com.econ.springboot.tdengine.demo.multi.datasource.domain;

import lombok.*;

/**
 * <p>
 * ems簇的配置信息表
 * </p>
 *
 * @author AutoGenerator
 * @since 2021-11-03
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class EmsClusterConfig{

    private static final long serialVersionUID = 1L;

    /**
     * 主键自增
     */
    private Integer id;

    /**
     * 簇id
     */
    private String deviceCode;

    /**
     * 配置的属性类别，参见ems_cluster_deatil_config表
     */
    private Integer type;


}
