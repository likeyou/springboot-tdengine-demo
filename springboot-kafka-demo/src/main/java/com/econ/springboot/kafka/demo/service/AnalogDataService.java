package com.econ.springboot.kafka.demo.service;

import com.econ.springboot.kafka.demo.domain.AnalogData;
import com.mongodb.BasicDBObject;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.index.Index;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author jwc
 * @date 2022/09/26
 */
@Service
@RequiredArgsConstructor
public class AnalogDataService {
    private final MongoTemplate mongoTemplate;

    /**
     * 将数据存入MongoDB
     * @param weathers
     */
    public void insertAll(List<AnalogData> weathers) {
        // insert(): 若新增数据的主键已经存在，则会抛异常提示主键重复，不保存当前数据。可批处理
        // save(): 若新增数据的主键已经存在，则会对当前已经存在的数据进行修改操作。
        mongoTemplate.insertAll(weathers);
    }

    public void insert(List<AnalogData> weathers) {
        // insert(): 若新增数据的主键已经存在，则会抛异常提示主键重复，不保存当前数据。可批处理
        // save(): 若新增数据的主键已经存在，则会对当前已经存在的数据进行修改操作。
        mongoTemplate.insert(weathers, "analog_data");
    }

    /**
     * 查找Story集合的所有文档
     * @return
     */
    public List<AnalogData> findAllWeather(){
        return mongoTemplate.findAll(AnalogData.class);
    }

    public void createMongoDBIndex(String collectionName) {
        if (!mongoTemplate.collectionExists(collectionName)) {
            BasicDBObject basicDBObject = new BasicDBObject();
            basicDBObject.put("pid", 1);
            mongoTemplate.createCollection(collectionName).createIndex(basicDBObject);
        }else {
            Index index = new Index();
            index.on("pid", Sort.Direction.ASC);
            mongoTemplate.indexOps(collectionName).ensureIndex(index);
        }
    }
}
