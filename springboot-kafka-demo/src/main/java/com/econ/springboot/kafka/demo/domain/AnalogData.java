package com.econ.springboot.kafka.demo.domain;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Accessors(chain = true)
@Document("analog_data")
public class AnalogData {


    private String id;

    private String pid;

    private Integer m;

    private String v;
}
