package com.econ.springboot.kafka.demo.dao;

import com.econ.springboot.kafka.demo.domain.AnalogData;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface AnalogDataRepository extends MongoRepository<AnalogData, String> {
}
