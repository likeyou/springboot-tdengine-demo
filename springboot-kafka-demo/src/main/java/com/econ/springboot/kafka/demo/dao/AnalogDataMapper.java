package com.econ.springboot.kafka.demo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.econ.springboot.kafka.demo.domain.AnalogData;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface AnalogDataMapper extends BaseMapper<AnalogData> {

    @Insert("<script>insert into analog_data VALUES <foreach collection='list' item='obj' separator=','>(#{obj.id}, #{obj.pid}, #{obj.m}, #{obj.v})</foreach></script>")
    int insertBatch(@Param("list") List<AnalogData> list);

    @Results({
            @Result(property = "ts", column = "last_row(ts)"),
            @Result(property = "id", column = "last_row(id)"),
            @Result(property = "m", column = "last_row(m)"),
            @Result(property = "v", column = "last_row(v)"),
            @Result(property = "pid", column = "last_row(pid)")
    })
    @Select("select last_row(*),last_row(pid) from analog_data group by pid")
    List<AnalogData> selectLastRowGroupByPid();
}
