package com.econ.springboot.kafka.demo;

import com.alibaba.fastjson.JSON;
import com.econ.springboot.kafka.demo.domain.AnalogData;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@SpringBootTest
class SpringbootKafkaDemoApplicationTests {

    // 这里是种子字母 当然如果需要可以加上数字 已经把大小写区分不明显的去掉了
    public static Character[] words = new Character[]{'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l' ,'m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
            'A', 'B', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};

    @Autowired
    KafkaTemplate kafkaTemplate;

    @Test
    void contextLoads() {
    }

    /**
     * 向kafka发送一条消息
     */
    @Test
    void testSendMsg() {
        Random random = new Random();
        String pid = getRandomWords(3, 2) + "." + getRandomWords(3, 2);
        AnalogData analogData = new AnalogData().setId(pid + "." + System.currentTimeMillis() + random.nextInt(100)).setPid(pid).setM(100).setV("1.00");
        kafkaTemplate.send("test1-jwc", 1, "", JSON.toJSONString(Lists.newArrayList(analogData)));
    }

    /**
     * 获取随机字母组合
     * begin 最小长度
     * offset 最小长度之后随机增加长度区间
     * @return
     */
    public static String getRandomWords(Integer begin, Integer offset) {
        //创建random 需要一个种子 同样的种子会出现固定顺序的random
        // 突发奇想用了时间戳
        Random random = new Random(System.currentTimeMillis());
        // 计算最终返回长度 这个方法是包左不包右的所以+1
        int i = random.nextInt(offset + 1) + begin;
        // 返回结果预存集合
        List<Character> results = new ArrayList<>();
        while(results.size() < i) {
            // 数组中取出一个随机索引 以及元素
            int index=(int)(Math.random()*words.length);
            results.add(words[index]);
        }
        // list 转character数组
        Character[] array = results.toArray(new Character[]{});
        // character 数组转char
        char[] chars = ArrayUtils.toPrimitive(array);
        // 返回结果
        return new String(chars);
    }
}
