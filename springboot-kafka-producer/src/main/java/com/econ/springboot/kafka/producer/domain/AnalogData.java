package com.econ.springboot.kafka.producer.domain;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class AnalogData {


    private String id;

    private String pid;

    private Integer m;

    private String v;
}
