package com.example.springboot.tdengine.demo.mybatisplus.mapper.mysql;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.springboot.tdengine.demo.mybatisplus.domain.EmsClusterConfig;

/**
 * <p>
 * ems簇的配置信息表 Mapper 接口
 * </p>
 *
 * @author AutoGenerator
 * @since 2021-11-03
 */
public interface EmsClusterConfigMapper extends BaseMapper<EmsClusterConfig> {

}
