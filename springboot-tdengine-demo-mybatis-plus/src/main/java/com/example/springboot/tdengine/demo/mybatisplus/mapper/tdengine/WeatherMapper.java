package com.example.springboot.tdengine.demo.mybatisplus.mapper.tdengine;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.springboot.tdengine.demo.mybatisplus.domain.Weather;
import org.apache.ibatis.annotations.Mapper;

public interface WeatherMapper extends BaseMapper<Weather> {

}
