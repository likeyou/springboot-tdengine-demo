package com.example.springboot.tdengine.demo.mybatisplus.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.*;

/**
 * <p>
 * ems簇的配置信息表
 * </p>
 *
 * @author AutoGenerator
 * @since 2021-11-03
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class EmsClusterConfig extends Model {

    private static final long serialVersionUID = 1L;

    /**
     * 主键自增
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 簇id
     */
    private String deviceCode;

    /**
     * 配置的属性类别，参见ems_cluster_deatil_config表
     */
    private Integer type;


}
