package com.example.springboot.tdengine.demo.mybatisplus.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.example.springboot.tdengine.demo.mybatisplus.domain.Weather;
import com.example.springboot.tdengine.demo.mybatisplus.mapper.tdengine.WeatherMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Author : zjf
 * @Description :
 * @Date : 2022/10/9 14:55
 * @Modified by : zjf
 */
@RestController
@RequestMapping(value = "/api/v2/test")
public class TestController {

    @Autowired
    WeatherMapper weatherMapper;

    @GetMapping
    public String test(){
        LambdaQueryWrapper<Weather> wrapper = Wrappers.<Weather>lambdaQuery().eq(Weather::getGroupid, 0);
        List<Weather> weathers = weatherMapper.selectList(wrapper);
        weathers.forEach(System.out::println);
        return "SUCCESS";
    }
}
